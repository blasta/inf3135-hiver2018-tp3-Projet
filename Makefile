CC = gcc 
CFLAGS  = -g -Wall `pkg-config --cflags jansson`
LIBS = $(shell pkg-config --libs jansson)
LFLAGS = $(shell pkg-config --libs cunit)


OBJECTS = tp3.o main.o test_main.o utils.o

tp3: main.o parse_args.o utils.o
	$(CC) $(CFLAGS) -o ./bin/tp3 main.o parse_args.o utils.o -ljansson
	

main.o: ./src/main.c
	$(CC) $(CFLAGS) -c  ./src/main.c

utils.o: ./src/utils.c
	$(CC) $(CFLAGS) -c  ./src/utils.c
	
test.o: ./test/test.c
	$(CC) $(CFLAGS) -c  ./test/test.c
	
parse_args.o: ./src/parse_args.c
	$(CC) $(CFLAGS) -c  ./src/parse_args.c
clean:
	-rm -f tp3.sh
	-rm -f *.o
	-rm -f ./bin/tp3.sh
