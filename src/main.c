/**
 * I N F 3 1 3 5  -   g r o u p e   10   -   HIV2018   -   T P 3
 *
 * 
 * TP3 Ce travail personnel consiste à produire l'Affichage des informations d'un pays entré 
 * en paramètre à partir d'un fichier json fournis "countries.json".
 *
 * @author (CADET MOZART)
 * @version (25-04-2018)
 *
 * (code permanent: CADM11078405)
 * (adresse courriel: cadet.mozart@courrier.uqam.ca)
 *
 *
 * professeur :  Rachid Kadouche
 * couriel : rachid.kadouche@uqam.ca
 *
 * I N F3 1 3 5  -   g r o u p e   10   -   HIV2018   -   T P 3
 * */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <jansson.h>
#include <getopt.h>
#include <string.h>
#include "parse_args.h"
#include "utils.h"
#include <string.h>
#include <assert.h>



/*
 * 
 */
int main(int argc, char** argv) {
    struct Arguments *arguments , argu ; 
    arguments  = &argu; 
    
    json_t *json_array_file;
    json_t *country_object ; 
    json_t *object1 ; 
    json_t *object2 ; 
    json_t *object3 ; 
    json_error_t error;
    char *key=NULL;
    
    json_t  *borders1 = NULL ;; 
    json_t  *borders2 = NULL ;; 
    json_t  *borders3 = NULL ; 
    
    
    json_array_file = json_load_file("./countries.json", 0, &error);
    
    setDefault_flag(arguments);
    
    
    if (argc!=1){
        *arguments = parseArguments(argc, argv);

        if (arguments->status != TP3_OK) {
            return arguments->status;
        } 

        if (arguments->flag_country){
            key = arguments->countryName;
            convert_toUpper(key);
            country_object = getJson_arrayObject(json_array_file,key);
            printf("\n");
            listJson_object(country_object,1,arguments);
            printf("\n");
        }else if(arguments->flag_region){
            key = arguments->regionName;
            getJson_regions(json_array_file,key,arguments);
            printf("\n");
        }else if(arguments->flag_sameBorders){
            

            printf("\n");
            
            key = arguments->pays_border1;
            convert_toUpper(key);
            object1 = getJson_arrayObject(json_array_file,key);
            borders1 = json_object_get(object1, "borders");
            
            
            key = arguments->pays_border2;
            convert_toUpper(key);
            object2 = getJson_arrayObject(json_array_file,key);
            borders2 = json_object_get(object2, "borders");
            
            
            if (strcmp(arguments->pays_border3,"\0")){
            key = arguments->pays_border3;
            convert_toUpper(key);
            object3 = getJson_arrayObject(json_array_file,key);
            borders3 = json_object_get(object3, "borders");
            
            }
            
            compare_array(borders1,borders2,borders3);
            printf("\n");
        }else if(arguments->flag_sameLanguage){
            
            json_t  *object_values1 = json_array();  // Pays 1 
            json_t  *object_values2= json_array();   // Pays 2 
            json_t  *object_values3 = json_array();  // pAYS  3 

            key = arguments->pays_border1;
            convert_toUpper(key);
            object1 = getJson_arrayObject(json_array_file,key);
            borders1 = json_object_get(object1, "languages");
            get_object_same(borders1,object_values1);
            
            key = arguments->pays_border2;
            convert_toUpper(key);
            object2 = getJson_arrayObject(json_array_file,key);
            borders2 = json_object_get(object2, "languages");
            get_object_same(borders2,object_values2);
           
            if (strcmp(arguments->pays_border3,"\0")){
            key = arguments->pays_border3;
            convert_toUpper(key);
            object3 = getJson_arrayObject(json_array_file,key);
            borders3 = json_object_get(object3, "languages");
            get_object_same(borders3,object_values3);            
            }

            compare_array(object_values1,object_values2,object_values3); // compare les 3 tableau de json_string  / Pays 1 ,  pays 2 , pays  3 
            printf("\n");
        }
    }
    return (EXIT_SUCCESS);
}

