/**
 * File parse_args.c
 *
 * Implements parse_args.h, using the getopt library to simplify the
 * processing.
 *
 * @author Alexandre Blondin Masse
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "parse_args.h"
#include <string.h>


/* Flag set by ‘--verbose’. */



// -------------- //
// Public methods //
// -------------- //

void printUsage(char **argv) {
    printf(USAGE, argv[0]);
}

struct Arguments parseArguments(int argc, char **argv) {
    bool showHelp = false;
    struct Arguments arguments;

    // Default argument
    arguments.status = TP3_OK;


    struct option longOpts[] = {
        // Set flag
        {"same-language", optional_argument , 0, 'm'},
        {"same-borders", optional_argument , 0, 's'},
        {"region",     required_argument, 0, 'r'},
        {"show-borders",     no_argument, 0, 'b'},
        {"show-languages",  no_argument, 0, 'l'},
        {"show-capital",  no_argument, 0, 'q'},
        {"country",  required_argument, 0, 'c'},
        {0, 0, 0, 0}
    };
    arguments.count = 0 ; 
    arguments.flag_sameBorders = 0 ; 
    arguments.flag_region = 0 ; 
    arguments.flag_borders = 0 ; 
    arguments.flag_capital = 0 ;
    arguments.flag_capital = 0 ; 
    arguments.flag_country = 0 ; 
    arguments.flag_languages = 0 ;    
    
    // Parse options
    while (true) {
        int option_index = 0;
        int c = getopt_long(argc, argv, "s:b:l:q:c:", longOpts, &option_index);
        if (c == -1) break;
        switch (c) {
            case 'm': if (arguments.status == TP3_OK){
                        arguments.flag_sameLanguage = 1 ;
                        
                        int index = 2 ; 
                        char *next;
                        while(index < argc){
                            next = strdup(argv[index]); /* get info */
                            strcpy(arguments.pays_border1+((index-2)*20), next);
                            arguments.count++ ; 
                            index++;
                            optind = index - 1;
                        }
                     }
                      break;
            case 's': if (arguments.status == TP3_OK){
                        arguments.flag_sameBorders = 1 ;

                        int index = 2 ; 
                        char *next;
                        while(index < argc){
                            next = strdup(argv[index]); /* get info */
                            strcpy(arguments.pays_border1+((index-2)*20), next);
                            arguments.count++ ; 
                            index++;
                            optind = index - 1;
                        }
                     }
                      break;
            case 'r': if (arguments.status == TP3_OK)
                        arguments.flag_region = 1 ;
                        strcpy(arguments.regionName, optarg);
                      break;
            case 'b': if (arguments.status == TP3_OK)
                        arguments.flag_borders = 1 ;
                      break;
            case 'l': if (arguments.status == TP3_OK)
                         arguments.flag_languages = 1 ;   
                      break;
            case 'q': if (arguments.status == TP3_OK)
                        arguments.flag_capital = 1 ;
                       break;
            case 'c': if (arguments.status == TP3_OK)
                         arguments.flag_country = 1 ; 
                        strcpy(arguments.countryName, optarg);
                        break;
            case '?': arguments.status = TP3_ERROR_BAD_OPTION;
                      break;
        }
    }

    
    if (arguments.flag_sameBorders | arguments.flag_sameLanguage ){
        if (arguments.count > 3){
            printf("Error: too many arguments\n");
            arguments.status = TP3_ERROR_TOO_MANY_ARGUMENTS;
        }
    } else if (optind < argc) {
        printf("Error: too many arguments\n");
        printUsage(argv);
        arguments.status = TP3_ERROR_TOO_MANY_ARGUMENTS;
    } else if (showHelp) {
        printUsage(argv);
        exit(TP3_OK);
    } else if (arguments.status == TP3_TYPE_ERROR) {
        printf("Error: the number of rows and columns must be integers\n");
        printUsage(argv);
    }
    return arguments;
}
