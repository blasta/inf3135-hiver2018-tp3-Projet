/**
 * Fichier parse_args.h
 *
 * Provides basic services to process the main arguments. Error codes are also
 * provided in the case of invalid input from the user.
 *
 * @author Mozart Cadet  basé sur le code de Alexandre Blondin Masse
 */
#include <string.h>
#ifndef PARSE_ARGS_H
#define PARSE_ARGS_H

#include <stdbool.h>


#define COUNTRY_LENGTH 20
#define USAGE "\
Usage: [--country  value] [--show-capital] [--show-languages ]\n\
    [--show-borders ] [--same-borders] [--same-language]\n\
\n\
Generates a random maze on the square grid.\n\
\n\
Optional arguments:\n\
  --country  value         Affiche le pays passer en paramètre.\n\
  --show-capital           Affiche la capital du pays passer en paramètre.\n\
  --show-languages         Affcihe la langue du pays passer en paramètre.\n\
  --show-borders           Affiche les frontière du pays passer en paramètre.\n\
                           The default value is bottom right corner.\n\
  --same-borders           Affiche les frontière similaires des pays entré en paramètre.\n\
  --same-language          Affiche les langues similaires des pays entrés en paramètre.\n\
"

/**
 * Liste de tout les code d'erreur produit par le programme.
 */
enum Error {
    TP3_OK                                = 0,
    TP3_ERROR_TOO_MANY_ARGUMENTS          = 1,
    TP3_TYPE_ERROR                        = 2,
    TP3_ERROR_BAD_OPTION                  = 3,
};

/**
 * Structure Argument contenant des flags indentifiant les éléments désirés
 * en paramètre.
 * Cette structure contient également 3 tableau non initialisé afin de contenir
 * une liste des langue/frontière des pays entré en paramètre.
 */
struct Arguments {   
    int  count ; // User arguments
    char pays_border1[COUNTRY_LENGTH]; 
    char pays_border2[COUNTRY_LENGTH]; 
    char pays_border3[COUNTRY_LENGTH]; 
    char flag_sameBorders ; 
    char flag_sameLanguage ; 
    char flag_region;
    char flag_borders;
    char flag_languages;
    char flag_capital;
    char flag_country;
    char countryName[COUNTRY_LENGTH]; // The output filename
    char regionName[COUNTRY_LENGTH]; 
    enum Error status;                    // The status of the parsing
    
};

/**
 * Prints how to use the program.
 *
 * @param argv  The arguments provided by the user
 */
void printUsage(char **argv);

/**
 * Parses the arguments provided by the user.
 *
 * @param argc  The number of arguments including the program name
 * @param argv  The arguments provided by the user
 * @return      The parsed arguments
 */
struct Arguments parseArguments(int argc, char **argv);

#endif
