/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <jansson.h>
#include <assert.h>
#include "parse_args.h"
#include "utils.h"


void process_Json(struct json_t *value,const char *key, int on,struct Arguments *arguments){
    
    if (json_is_string(value)){
        if (!on) {
            printf( "%s",json_string_value(value));
        }else{
            if (strcmp(key,"cca3"))
                     printf( "Code : %s\n",json_string_value(value));
            else
                printf( "Name : %s\n",json_string_value(value));
        }
   
    }else if (json_is_array(value)){
        if (!strcmp(key,"borders") && arguments->flag_borders){
            printf("Borders: ");
            listJson_array(value,on,arguments);
        }
        if (!strcmp(key,"capital") && arguments->flag_capital){
            printf("Capital: ");
            listJson_array(value,on,arguments);
            printf("\n");
        }
    }
}
json_t *getJson_arrayObject(struct json_t* json_array, char *key){
    
    
        size_t index;
        json_t *array_object;
        json_t *value;
        const char *jsonKey ; 
        const char *jsonValue ; 
        
        json_array_foreach(json_array, index, array_object) {
            if (!json_is_string(array_object)){
              void *iter  =  json_object_iter(array_object);
                while(iter)
                {
                     jsonKey = json_object_iter_key(iter);
                     value = json_object_iter_value(iter);
                     if ((strcmp(jsonKey,"cca3")==0) & json_is_string(value) ){
                         jsonValue = json_string_value(value) ; 
                         if (strcmp(jsonValue,key)==0){
                            return array_object ; 
                         }                     
                         iter = json_object_iter_next(array_object, iter);
                     }else{
                        iter = json_object_iter_next(array_object, iter);
                     }
                } 
            }
        }
        return 0 ; 
}

int getJson_regions(struct json_t* json_array, char *key,struct Arguments *arguments){

        size_t index;
        json_t *array_object;
        json_t *value;
        const char *jsonKey ; 
        const char *jsonValue ;
        
        if (!json_array){
          return 0 ; 
        }
        json_array_foreach(json_array, index, array_object) {
            if (!json_is_string(array_object)){
              void *iter  =  json_object_iter(array_object);
                while(iter)
                {
                     jsonKey = json_object_iter_key(iter);
                     value = json_object_iter_value(iter);
                     if ((strcmp(jsonKey,"region")==0) & json_is_string(value) ){
                         jsonValue = json_string_value(value) ; 
                         if (strcmp(jsonValue,key)==0){                         
                             listJson_object(array_object,1,arguments);
                             printf("..............\n");
                         }                     
                         iter = json_object_iter_next(array_object, iter);
                     }else{
                        iter = json_object_iter_next(array_object, iter);
                     }
                } 
            }
        }
        return 1 ; 
}

int listJson_array(struct json_t* json_array,int on,struct Arguments *arguments){
    if(json_array) {
        const char *key2 ; 
        size_t index;
        json_t *array_object;
        json_t *value;

        json_array_foreach(json_array, index, array_object) {
            if (!json_is_string(array_object)){
                void *iter  =  json_object_iter(array_object);
                while(iter)
                {
                    key2 = json_object_iter_key(iter);
                    if ((!strcmp(key2,"name")) || (!strcmp(key2,"cca3")) || ((!strcmp(key2,"capital")) && arguments->flag_capital)|| ((!strcmp(key2,"languages")) && arguments->flag_languages) || ((!strcmp(key2,"borders")) && arguments->flag_borders)){
                    value = json_object_iter_value(iter);
                    (!(strcmp(key2,"name")==0))? printf( "\n%s ",key2) : printf( "\n\n%s ",key2);   
                    if ((strcmp(key2,"languages")==0)){
                                json_t *value2;
                                json_object_foreach(value, key2, value2) {
                                     printf( " %s",json_string_value(value2));
                                }
                    }
                     if (!json_is_string(value)){
                        json_is_object(value) ? listJson_object(value,on,arguments) :listJson_array(value,on,arguments);
                     }else{
                       printf( " %s",json_string_value(value));     
                     }
                    }
                    iter = json_object_iter_next(array_object, iter);
                } 
            }else{
                     printf( "%s ",json_string_value(array_object));  
            }
        }
    }else{
        return 0; 
    }
    return 1 ; 
}



void listJson_object(struct json_t* json_object, const int on ,struct Arguments *arguments){
    if(json_is_object(json_object)) {
        json_t *value;
            void *iter  =  json_object_iter(json_object);
            while(iter)
            { 
                const char *key3;
                key3 = json_object_iter_key(iter);
                if ((!strcmp(key3,"official")) || ((!strcmp(key3,"languages")) && arguments->flag_languages) || ((!strcmp(key3,"borders")) && arguments->flag_borders) || (!strcmp(key3,"name")) || (!strcmp(key3,"cca3")) || ((!strcmp(key3,"capital")) && arguments->flag_capital)  ){
                value = json_object_iter_value(iter);
                    if ((!strcmp(key3,"languages"))){
                                json_t *value2;
                                printf("Languages :"); 
                                json_object_foreach(value, key3, value2) {
                                    printf( " %s",json_string_value(value2));
                                }
                                printf("\n"); 
                    }else{
                        json_is_object(value) ? listJson_object(value,on,arguments) : process_Json(value,key3,on,arguments);
                    }
                }
                iter = json_object_iter_next(json_object, iter);
            }
    }
}

int convert_toUpper(char *temp) {
  if (!temp){
     return 0 ; 
  }else{
  char * name;
  name = strtok(temp,":");

  // Convert to upper case
  char *s = name;
    while (*s) {
      *s = toupper((unsigned char) *s);
      s++;
    }
  }
    return 1 ; 
}

void compare_array(json_t* array1 , json_t* array2, json_t* array3) {
    size_t index;
    json_t *value;
    size_t index2;
    json_t *value2;
    size_t index3;
    json_t *value3;
    char same_borders[50] = {}; 

   json_array_foreach(array1, index, value){
            

       json_array_foreach(array2, index2, value2){  // il compare au moin 2  array 
           

           if (json_array_size(array3)){  // retourne = 0 si l'array est vide sinon retoourne ok 
                  json_array_foreach(array3, index3, value3){
                      

                      if (json_equal(value,value2) ){
                          
                          
                         if  (json_equal(value,value3)){
                             
                                     strcat(same_borders,json_string_value(value)); 

                          }
                      }


                     }
                  
           }else{
                  if(json_equal(value,value2)){ 
                      strcat(same_borders,json_string_value(value)); 
                  }
           }


       }
              
  }
   
   if (strlen(same_borders)){
       printf("Yes %s\n", same_borders);
   }else {
       printf("No ");
   }

}


void setDefault_flag(struct Arguments* arguments){
    arguments->flag_borders = 1 ; 
    arguments->flag_capital = 1 ;
    arguments->flag_capital = 1 ; 
    arguments->flag_country = 1 ; 
    arguments->flag_languages = 1 ; 
}

json_t* get_object_same(const struct json_t* object, struct json_t* object_values){
    size_t index;
    json_t *value;
    

    
     //  Json_object  ==  a l'objet language  
    json_object_foreach(object, index, value){
        json_array_append_new(object_values, value);
    }    
    
    return object_values ; 
}