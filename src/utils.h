/**
 * Fichier utils.h
 *
 * Fournis des fonctions de manipulations de base et  utile sur des données 
 * en générale.
 * 
 *
 * Un fichier JSON est fournis comme entré. Ce fichier contient tous les pays
 * du monde qui y sont représenté comme des objets json. Plusieurs manipulations 
 * utililent sont effectuées sur ses objets et les fonctions d'affichage ou de
 * manipulation sur ceux-ci sont contenu dans ce fichier.
 *
 * 
 *
 * @author Mozart Cadet
 */

#ifndef UTILS_H
#define UTILS_H

/**
 * Ajoute les flags par default à la structure arguments
 *
 * @param arguments   arguments de base de notre structure
 *
 * par défaut tout les fanions sont a "On" ce qui permet d'afficher tout les 
 * paramètres d'un pays
 */
void setDefault_flag(struct Arguments* arguments);

/**
 * Affiche l'argument si c'est un string sinon renvoi à la fonction list_array()
 * 
 * @param value       struc json_t soit un json_array() soit un json_string                        
 * @param arguments   arguments de base de notre structure avec les fanions     
 *                    activé.
 * @param key         La clé de l'objet en question     
                           
 *  par défaut tout les fanions sont a "On" ce qui permet d'afficher toutes les 
 *  informations. Re-dirige en fonction de l'argument qui ne peut être un objet vers 
 *  list_array(argument) si c'est un tableau ou affiche directement à la console
 *  ca valeur comme il ne peut qu'être un json_string 
 */
void process_Json(struct json_t *value,const char *key, int on,struct Arguments *arguments);


/**
 * Returns un pays la l'abréviation est la "key"
 *
 * En somme cette fonction retourne un objet de notre json_array principal.
 * Notre tableau principal contient des pays donc un pays est retourné par
 * cette fonction
 *
 * @param json_array Un pointeur vers un json_array "Tableau de pays"
 * @param key        Un pointeur vers une chaine de charactère contanant l'abré-
 *                   viation d'un pays.
 * @return            country_object
 */
json_t *getJson_arrayObject(struct json_t* json_array, char *key) ;


/**
 * Retourne Vrai si on lui passe un json_array sinon retourne Faux
 *
 * Cette fonction affiche les objects contenues dans un tableau json et retourne
 * vrai si on lui a belle et bien passé un tableau sinon retourne faux.
 * 
 * @param json_array  un pointeur vers un json_array
 * @return TRUE/FALSE
 */
int listJson_array(struct json_t* json_array,int on,struct Arguments *arguments);


/**
 * Affiche l'argument si c'est un objet sinon renvoi à la fonction
 * process_json()
 * 
 * @param json_object       struc json_t soit un json_array() soit un json_string                        
 * @param on   arguments de base de notre structure avec les fanions     
 *                    activé.
 * @param arguments   arguments de base de notre structure avec les fanions     
 *                    activé.    
 * 
 *  Si le fanion de l'argument est a on et que la clé rechercher est trouvé 
 *  alors on affiche la valeur de la clé
 */
void listJson_object(struct json_t* json_object, const int on ,struct Arguments *arguments) ;


/**
 * Retourne Vrai si on lui passe un json_array et liste la la nom du 
 * pays apparteanant à la région. dans le cas contraire la fonction retourne 
 * Faux
 *
 * 
 * @param json_array  un pointeur vers un json_array
 * @param key         Un pointeur vers une chaine de charactère contanant l'abré-
 *                    viation d'un pays.
 * @return TRUE/FALSE
 */
int getJson_regions(struct json_t* json_array, char *key,struct Arguments *arguments) ; 


/**
 * Compare trois tableau passé en paramètre et affiche les éléments
 * similaire
 * 
 * @param array1       struc json_t,  un json_array()   
 * @param array2       struc json_t,  un json_array()                       
 * @param array3       struc json_t,  un json_array()                                           
 *                 
 * Compare au mininum deux tableau et retourne les éléments similaires 
 */
void compare_array(json_t* array1 , json_t* array2, json_t* array3) ; 


/**
 * Convertit une chaine de charactère en majuscule
 *
 * 
 * @param temp  Un pointeur vers une chaine de charactère contanant 
 *              l'abréviation d'un pays.
 * 
 * @return temp
 */
int convert_toUpper(char *temp);


/**
 * Retourne Un tableau json_array contenant les objets qui on soit le mêmes 
 * langue ou les mêmes frontières
 * 
 * @param object  un pointeur vers un json_array
 * @param object_values  tableau de langues ou de fonctières
 */
json_t* get_object_same(const struct json_t* object, struct json_t* object_values);

#endif /* UTILS_H */

