/**
 * I N F 3 1 3 5  -   g r o u p e   10   -   HIV2018   -   T P 3
 *
 *
 * @author (CADET MOZART)
 * @version (25-04-2018)
 *
 * (code permanent: CADM11078405)
 * (adresse courriel: cadet.mozart@courrier.uqam.ca)
 *
 *
 * professeur :  Rachid Kadouche
 * couriel : rachid.kadouche@uqam.ca
 *
 * I N F3 1 3 5  -   g r o u p e   10   -   HIV2018   -   T P 3
 * */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <CUnit/Basic.h>
#include <getopt.h>
#include <jansson.h>
#include <string.h>
#include "../src/parse_args.h"
#include "../src/utils.h"


/*
 * CUnit Test Suite
 */

int init_suite(void) {
    return 0;
}

int clean_suite(void) {
    return 0;
}

void test1() {
    struct Arguments *arguments = (struct Arguments*)malloc(sizeof(arguments));
    arguments->count = 5; 
    strcpy(arguments->countryName,"CAN");
    arguments->flag_borders = 1 ; 
    arguments->flag_capital = 1 ; 
    
    
    CU_ASSERT_FALSE_FATAL(arguments->flag_sameBorders);
    CU_ASSERT_FALSE_FATAL(arguments->flag_country);
    CU_ASSERT_FALSE_FATAL(arguments->flag_languages);
    CU_ASSERT_FALSE_FATAL(arguments->flag_region);
    CU_ASSERT_FALSE_FATAL(arguments->flag_sameLanguage);
    CU_ASSERT_TRUE_FATAL(arguments->flag_borders);
    CU_ASSERT_TRUE_FATAL(arguments->flag_capital);


}

void test_getJson_arrayObject(void)
{
    json_error_t error;
    char *key=NULL;
    json_t *json_array_file = json_load_file("./countries.json", 0, &error);
    
    struct Arguments *arguments , argu ; 
    arguments  = &argu; 
    
    jmp_buf buf;
    int i;

    i = setjmp(buf);
    if (i == 0) {
      getJson_arrayObject(json_array_file,"can");
      CU_PASS("run_other_func() succeeded.");
    }else
      CU_FAIL("run_other_func() issued longjmp.");
 }


void test_getJson_arrayObject_null(void)
{
    if (getJson_arrayObject(NULL,"can")) {
      CU_FAIL("run_other_func() issued longjmp.");
    }else
      CU_PASS("run_other_func() succeeded.");
    
}

void test_listJson_object(void)
{

    json_error_t error;
    char *key=NULL;
    json_t *json_array_file = json_load_file("./countries.json", 0, &error);
    
    struct Arguments *arguments , argu ; 
    arguments  = &argu; 
    json_t *country = getJson_arrayObject(json_array_file,"can");

    
    jmp_buf buf;
    int i;

    i = setjmp(buf);
    if (i == 0) {
      listJson_object(country,0,arguments);
      CU_PASS("run_other_func() succeeded.");
    }else
      CU_FAIL("run_other_func() issued longjmp.");
    
}

void test_listJson_object_null(void)
{

    struct Arguments *arguments , argu ; 
    arguments  = &argu; 

    jmp_buf buf;
    int i;

    i = setjmp(buf);
    if (i == 0) {
      listJson_object(NULL,0,arguments);
      CU_PASS("run_other_func() succeeded.");
    }else
      CU_FAIL("run_other_func() issued longjmp.");
    
}

void test_getJson_regions(void)
{

    json_error_t error;
    json_t *json_array_file = json_load_file("./countries.json", 0, &error);
    
    struct Arguments *arguments , argu ; 
    arguments  = &argu; 
    json_t *country = getJson_arrayObject(json_array_file,"can");

    
    jmp_buf buf;
    int i;

    i = setjmp(buf);
    if (i == 0) {
      getJson_regions(json_array_file,"oceania",arguments);
      CU_PASS("run_other_func() succeeded.");
    }else
      CU_FAIL("run_other_func() issued longjmp.");
}

void test_getJson_regions_null(void)
{
    struct Arguments *arguments , argu ; 
    arguments  = &argu; 

    if (getJson_regions(NULL,"oceania",arguments)) {
        CU_FAIL("run_other_func() issued longjmp.");

    }else
        CU_PASS("run_other_func() succeeded.");

}

void test_listJson_array(void)
{

    json_error_t error;
    char *key=NULL;
    json_t *json_array_file = json_load_file("./countries.json", 0, &error);
    
    struct Arguments *arguments , argu ; 
    arguments  = &argu; 
    json_t *country = getJson_arrayObject(json_array_file,"can");

    
    jmp_buf buf;
    int i;

    i = setjmp(buf);
    if (i == 0) {
      listJson_array(country,0,arguments);
      CU_PASS("run_other_func() succeeded.");
    }else
      CU_FAIL("run_other_func() issued longjmp.");
    
}

void test_listJson_array_null(void)
{
    struct Arguments *arguments , argu ; 
    arguments  = &argu; 
    
    if (listJson_array(NULL,"oceania",arguments)) {
        CU_FAIL("run_other_func() issued longjmp.");
    }else
        CU_PASS("run_other_func() succeeded.");
    
}

void test_convert_toUpper() {
    
    char test_temp[]= "test"; 
    char* test = test_temp;
    
    if (!convert_toUpper(test)) {
        CU_PASS("run_other_func() succeeded.");
    }else
        CU_FAIL("run_other_func() issued longjmp.");
    

}



int main() {
    CU_pSuite pSuite = NULL;

    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    pSuite = CU_add_suite("**** Test *****", init_suite, clean_suite);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if ((NULL == CU_add_test(pSuite, "test1", test1)) 
        || (NULL == CU_add_test(pSuite, "test_getJson_arrayObject", test_getJson_arrayObject))
        || (NULL == CU_add_test(pSuite, "test_getJson_arrayObject_null", test_getJson_arrayObject_null))
        || (NULL == CU_add_test(pSuite, "test_listJson_object", test_listJson_object))
        || (NULL == CU_add_test(pSuite, "test_listJson_object_null", test_listJson_object_null))
        || (NULL == CU_add_test(pSuite, "test_getJson_regions", test_getJson_regions)) 
        || (NULL == CU_add_test(pSuite, "test_getJson_regions_null", test_getJson_regions_null)) 
        || (NULL == CU_add_test(pSuite, "test_listJson_array", test_listJson_array))
        || (NULL == CU_add_test(pSuite, "test_listJson_array_null", test_listJson_array_null)) 
        || (NULL == CU_add_test(pSuite, "test_convert_toUpper", test_convert_toUpper))) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}
